import unittest
from debdrylib.tree import Control

def doctrim(docstring):
    """
    Trim a docstring-like string.
    """
    maxint = 9999
    # From http://legacy.python.org/dev/peps/pep-0257/#handling-docstring-indentation
    if not docstring:
        return ''
    # Convert tabs to spaces (following the normal Python rules)
    # and split into a list of lines:
    lines = docstring.expandtabs().splitlines()
    # Determine minimum indentation (first line doesn't count):
    indent = maxint
    for line in lines[1:]:
        stripped = line.lstrip()
        if stripped:
            indent = min(indent, len(line) - len(stripped))
    # Remove indentation (first line is special):
    trimmed = [lines[0].strip()]
    if indent < maxint:
        for line in lines[1:]:
            trimmed.append(line[indent:].rstrip())
    # Strip off trailing and leading blank lines:
    while trimmed and not trimmed[-1]:
        trimmed.pop()
    while trimmed and not trimmed[0]:
        trimmed.pop(0)
    # Return a single string:
    return '\n'.join(trimmed)


class TestCombine(unittest.TestCase):
    def _parse_control(self, buf):
        # TODO: deindent buf
        return Control.scan_string("control", "./control", buf)

    def assertCombines(self, c1, c2, expected):
        c1 = self._parse_control(doctrim(c1))
        c2 = self._parse_control(doctrim(c2))
        c = c1.combine("control", "./control", c2)
        self.assertEquals(c.to_string().strip(), doctrim(expected))

    def test_plainfield(self):
        self.assertCombines(
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@enricozini.org>
            """,
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@debian.org>
            """,
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@debian.org>
            """)

    def test_smartfield_plain(self):
        self.assertCombines(
            """
            Source: foo
            Build-Depends: python3-all, foo
            """,
            """
            Source: foo
            Build-Depends: python3-all, dh-python
            """,
            """
            Source: foo
            Build-Depends: python3-all, dh-python
            """)

    def test_smartfield_smart(self):
        self.assertCombines(
            """
            Source: foo
            Build-Depends: python3-all, foo
            """,
            """
            Source: foo
            X-Debdry-Build-Depends: python3-all, dh-python
            """,
            """
            Source: foo
            Build-Depends: python3-all, foo, dh-python
            """)

class TestDiff(unittest.TestCase):
    def _parse_control(self, buf):
        # TODO: deindent buf
        return Control.scan_string("control", "./control", buf)

    def assertDiffs(self, c1, c2, expected):
        c1 = self._parse_control(doctrim(c1))
        c2 = self._parse_control(doctrim(c2))
        c = c1.diff("control", "./control", c2)
        self.assertEquals(c.to_string().strip(), doctrim(expected))

    def test_plainfield(self):
        self.assertDiffs(
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@enricozini.org>
            """,
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@debian.org>
            """,
            """
            Source: foo
            Maintainer: Enrico Zini <enrico@debian.org>
            """)

    def test_smartfield_plain(self):
        self.assertDiffs(
            """
            Source: foo
            Build-Depends: python3-all, foo
            """,
            """
            Source: foo
            Build-Depends: python3-all, dh-python
            """,
            """
            Source: foo
            Build-Depends: python3-all, dh-python
            """)

    def test_smartfield_smart(self):
        self.assertDiffs(
            """
            Source: foo
            Build-Depends: python3-all, foo
            """,
            """
            Source: foo
            Build-Depends: python3-all, foo, dh-python
            """,
            """
            Source: foo
            X-Debdry-Build-Depends: dh-python
            """)
