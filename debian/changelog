debdry (0.2.2-2) UNRELEASED; urgency=medium

  * Move VCS to salsa.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 19:49:06 +0100

debdry (0.2.2-1) unstable; urgency=medium

  * QA upload.

  [ Antonio Terceiro ]
  * git-debdry-build: switch from `git-buildpackage` to `gbp buildpackage`

  [ Paul Wise ]
  * Fix minor typo in the package description (Closes: #780254)
  * Use canonical links to anonscm.d.o repos
  * Add python3-apt to X-Debdry-Build-Depends (Closes: #805195)
  * Set Maintainer to orphaned (See: #813203)
  * Pass --git-ignore-new to gbp

 -- Paul Wise <pabs@debian.org>  Tue, 25 Oct 2016 09:19:23 +0800

debdry (0.2.1-1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add missing dependency on python3-apt (Closes: #779443)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 12 Mar 2015 20:03:46 -0300

debdry (0.2-1) unstable; urgency=medium

  [ Antonio Terceiro ]
  * New release
  * Rename binary package to debdry (Closes: #761961)
  * Add support for doing the initial debianisation with debdry.
  * Add new git-debdry-build binary which bridges debdry and git-buildpackage
    so that one can keep only the minimal debian/ in the master branch, and
    build on a separate branch containined the expanded debian/ directory (and
    cab be tagged for releases etc).
  * Add Vcs-* headers (Closes: #761962)

  [ Enrico Zini ]
  * Offer to preserve work directories when --debug is used and we are
    connected to a tty
  * Fixed parsing control files with package fields with trailing commas.
    Closes: #763316
  * Added debian/README.source to explain how the debian/ directory is special
    compared to other packages. Closes: #763317
  * Remove *.egg-info directories left around by setup.py

 -- Enrico Zini <enrico@debian.org>  Thu, 16 Oct 2014 19:07:21 +0200

debdry (0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #750576)

 -- Enrico Zini <enrico@debian.org>  Sat, 30 Aug 2014 19:07:50 -0700
