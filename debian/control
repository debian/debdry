Source: debdry
Priority: optional
Section: devel
Maintainer: Debian QA Group <packages@qa.debian.org>
Standards-Version: 3.9.6
Build-Depends: dh-python, python3-setuptools, python3-all, debhelper (>= 9), python3-apt
Homepage: https://anonscm.debian.org/cgit/collab-maint/debdry.git
Vcs-Browser: https://salsa.debian.org/debian/debdry.git
Vcs-Git: https://salsa.debian.org/debian/debdry.git

Package: debdry
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-apt
Conflicts: python3-debdry
Replaces: python3-debdry
Description: Semi-assisted automatic Debian packaging
 debdry is for debian/ directories what debhelper7 is for debian/rules.
 .
 It applies the Don't Repeat Yourself idea to packaging, attempting to reuse as
 much as possible of upstream's metadata and standard packaging practices.
 .
 debdry runs an appropriate auto-debianisation tool for a given source
 directory, then applies manual overrides from a debian.in directory.
 .
 debdry supports the following types of packages (in brackets you will find
 extra packages that need to be installed for each type of package):
 .
 - Perl [dh-make-perl]
 - Python [python-stdeb, python3-stdeb]
 - Ruby [gem2deb]
 - Haskell [cabal-debian]
 .
