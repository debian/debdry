# git-debdry-build

A simple wrapper around `gbp buildpackage` and debdry.

# Usage

$ git debdry-build [OPTIONS]

# Options

**git-debdry-build** does not take any options itself, and all options passed
to it on the command line are passed unchanged to `git-buildpackage`.

# Mode of operation

**git-debdry-build** will create a temporary branch off the current HEAD, run
debdry on it, commit the _debian/_ directory to that branch, and build the
package using **git-buildpackage(1)**.

If the build succeeds, you will find the build artifacts in the parent
directory as usual, and the temporary branch will be deleted. If you want to
save the state of tree used for the build (for example when doing the final
build before uploading to the Debian archive), you can just pass the
_--git-tag_ option to **git-buildpackage** and that very tree will be tagged
for you.

If the build fails, **git-debdry-build** will not remove the temporary branch
and will tell you its name so that you can inspect its contents if necessary.

# See also

**git-buildpackage(1)**
