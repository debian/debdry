#!/usr/bin/python3
# coding: utf8
#
# Copyright (C) 2014 Enrico Zini <enrico@enricozini.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from .auto import Auto
from .tree import DebianDirectory
import os
import shutil

class Debdry:
    def __init__(self, srcdir, debug=False):
        self.srcdir = srcdir
        self.debug = debug
        self.dir_backup = os.path.join(srcdir, "debian/debdry")
        self.dir_auto = os.path.join(srcdir, "debian.auto")
        self.dir_debian = os.path.join(srcdir, "debian")

    def is_debdry(self):
        """
        Check if the debian/ directory is in debdry state
        """
        return not os.path.exists(self.dir_backup)

    def is_debianised(self):
        """
        Check if the debian/ directory is in fully debianised state
        """
        return os.path.exists(self.dir_backup)

    def restore(self):
        """
        Remove all the contents of debian/ and replace them with the contents
        of debian/.debdry
        """
        if not self.is_debianised():
            raise RuntimeError("restore_debdry called on a non-debianised source")

        # Delete everything except .debdry
        for fn in os.listdir(self.dir_debian):
            if fn == "debdry": continue
            pathname = os.path.join(self.dir_debian, fn)
            if os.path.isdir(pathname):
                shutil.rmtree(pathname)
            else:
                os.unlink(pathname)

        # Move all in .debdry one dir up
        for fn in os.listdir(self.dir_backup):
            os.rename(os.path.join(self.dir_backup, fn), os.path.join(self.dir_debian, fn))

        # Remove .debdry
        os.rmdir(self.dir_backup)

    def debianise(self):
        """
        Debianise the package, backing up debdry originals in dir_backup
        """
        tmpdir = os.path.join(self.srcdir, "debian.debdry")
        if os.path.isdir(tmpdir): shutil.rmtree(tmpdir)

        if not os.path.exists(self.dir_debian):
            os.makedirs(self.dir_debian)

        # Move the debdry sources to debian.debdry
        if os.path.exists(self.dir_backup):
            os.rename(self.dir_backup, tmpdir)
            shutil.rmtree(self.dir_debian)
        else:
            os.rename(self.dir_debian, tmpdir)

        try:
            # Perform automatic debianisation
            Auto.debianise(self.srcdir)

            # Move debian/ to debian.auto/
            os.rename(self.dir_debian, self.dir_auto)

            auto = DebianDirectory.scan(self.srcdir, self.dir_auto)
            manual = DebianDirectory.scan(self.srcdir, tmpdir)
            merged = DebianDirectory.combine(self.srcdir, self.dir_debian, auto, manual)
        except:
            # Rollback: restore debian.debdry to debian
            self.maybe_preserve_workdir(tmpdir)
            if os.path.isdir(self.dir_debian): shutil.rmtree(self.dir_debian)
            os.rename(tmpdir, self.dir_debian)
            raise
        else:
            # Commit: move debian.debdry to debian/debdry
            os.rename(tmpdir, self.dir_backup)
        finally:
            if os.path.isdir(self.dir_auto): shutil.rmtree(self.dir_auto)

    def diff(self):
        """
        Take an existing, manually maintained debian/ dir, compute what it adds
        to an auto-generated package, and put the results in debian/debdry
        """
        tmpdir = os.path.join(self.srcdir, "debian.debdry")
        if os.path.isdir(tmpdir): shutil.rmtree(tmpdir)

        # Move debian to debian.debdry
        os.rename(self.dir_debian, tmpdir)

        # Remove debian.debdry/debdry if it exists
        debdrydir = os.path.join(tmpdir, "debdry")
        if os.path.exists(debdrydir):
            shutil.rmtree(debdrydir)

        try:
            # Perform automatic debianisation
            Auto.debianise(self.srcdir)

            # Move debian/ to debian.auto/
            os.rename(self.dir_debian, self.dir_auto)

            # Diff with debdry
            auto = DebianDirectory.scan(self.srcdir, self.dir_auto)
            manual = DebianDirectory.scan(self.srcdir, tmpdir)
            diffed = DebianDirectory.diff(self.srcdir, debdrydir, auto, manual)
        except:
            # Rollback: remove debian.debdry/debdry
            self.maybe_preserve_workdir(debdrydir)
            if os.path.isdir(debdrydir): shutil.rmtree(debdrydir)
            raise
        finally:
            # Restore debian.debdry back to debian
            os.rename(tmpdir, self.dir_debian)
            if os.path.isdir(self.dir_auto): shutil.rmtree(self.dir_auto)

    def maybe_preserve_workdir(self, pathname):
        """
        Offer to preserve a work directory during rollback, if self.debug is
        set and we are connected to a tty.
        """
        if not self.debug or not os.isatty(0) or not os.isatty(1): return
        if not os.path.isdir(pathname): return
        renamed_pathname = pathname + ".debug"
        want_rename = input(
            "An error occurred and --debug is used: preserve {} as {}? (y/N) ".format(
                pathname, renamed_pathname))
        if want_rename.lower() != "y": return

        if os.path.isdir(renamed_pathname):
            shutil.rmtree(renamed_pathname)
        # Hardlink the backup directory contents to the original directory
        shutil.copytree(pathname, renamed_pathname, copy_function=os.link)
