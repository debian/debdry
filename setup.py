#!/usr/bin/env python3

from setuptools import setup

setup(name="debdry",
      version="0.2.2",
      description="Semi-assisted automatic Debian packaging",
      long_description="""debdry is for debian/ directories what debhelper7 is for debian/rules.

It applies the Don't Repeat Yourself idea to packaging, attempting to reuse as
much as possible of upstream's metadata and standard packaging practices.

debdry runs an appropriate auto-debianisation tool for a given source
directory, then applies manual overrides from a debian.in directory.

debdry supports the following types of packages (in brackets you will find
extra packages that need to be installed for each type of package):

- Perl [dh-make-perl]
- Python [python-stdeb, python3-stdeb]
- Ruby [gem2deb]
- Haskell [cabal-debian]
""",
      author="Enrico Zini",
      author_email="enrico@debian.org",
      url="https://anonscm.debian.org/cgit/collab-maint/debdry.git",
      requires=["apt"],
      license="GPL3",
      platforms="any",
      packages=["debdrylib"],
      scripts=["debdry", "git-debdry-build"],
)
